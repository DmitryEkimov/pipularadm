﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using MySql.Web.Security;

namespace SimpleMembershipTest.Dac
{
    public class DropCreateDatabaseIfModelChangesInitializer : IDatabaseInitializer<SimpleMembershipTestDbContext>
    {
        public void InitializeDatabase(SimpleMembershipTestDbContext context)
        {
            //var needtochange = false;
            bool dbExists;
            //using (new TransactionScope(TransactionScopeOption.Suppress))
            //{
            dbExists = context.Database.Exists();
            //}
            if (dbExists)
            {
                var cmd = context.Database.Connection.CreateCommand();
                context.Database.Connection.Open();
                cmd.CommandText = @"select count(*) from sysobjects where xtype = 'U'";
                var tablecount = (int) cmd.ExecuteScalar();
                context.Database.Connection.Close();
                //var needToCreateDB = ConfigurationManager.AppSettings["NeedToCreateDB"].ToLower();
                //if (tablecount == 0 || needToCreateDB == "true")
                //{
                //needtochange = true;
                //}
                //try
                //{
                //    needtochange = !context.Database.CompatibleWithModel(true);
                //}
                //catch (Exception ex)
                //{
                //    needtochange = true;
                //}
            }
            else
            {
                context.Database.Create();
                //needtochange = true;
            }
        }
    }

    //    : DropCreateMySqlDatabaseIfModelChanges<SimpleMembershipTestDbContext>
    //{
    //    protected override void Seed(SimpleMembershipTestDbContext db)
    //    {
    //        db.UserProperties.Add(new UserProperty
    //        {
    //            UserId = 1,
    //            UserName = "admin",
    //            Age = 40,
    //            Email = "xyz3710@gmail.com",
    //            Facebook = "http://facebook.com/xyz37",
    //            Rate = 100,
    //            LastName = "Kim",
    //            FirstName = "Ki Won",
    //        });
    //    }
    //}
}
