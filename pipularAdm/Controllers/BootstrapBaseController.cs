﻿using System.Web.Mvc;
using pipularAdm.Helpers;
using SimpleMembershipTest.Dac;
using BootstrapSupport;

namespace pipularAdm.Controllers
{
    public class BootstrapBaseController: Controller
    {
        protected readonly SimpleMembershipTestDbContext _context;
        protected ISessionState _sessionstate;
        public BootstrapBaseController(SimpleMembershipTestDbContext context, ISessionState sessionstate)
        {
            _context = context;
            _sessionstate = sessionstate;
        }

        public void Attention(string message)
        {
            TempData.Add(Alerts.ATTENTION, message);
        }

        public void Success(string message)
        {
            TempData.Add(Alerts.SUCCESS, message);
        }

        public void Information(string message)
        {
            TempData.Add(Alerts.INFORMATION, message);
        }

        public void Error(string message)
        {
            TempData.Add(Alerts.ERROR, message);
        }
    }
}
