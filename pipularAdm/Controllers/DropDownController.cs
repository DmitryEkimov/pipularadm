﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using pipularAdm.Helpers;
using SimpleMembershipTest.Dac;

namespace pipularAdm.Controllers
{
    public class DropDownController : BootstrapBaseController
    {
        public DropDownController(SimpleMembershipTestDbContext context, ISessionState sessionstate)
            : base(context, sessionstate)
        {
        }

        public ActionResult GetRoles(string selectedId)
        {
            ViewData.Model = Roles.GetAllRoles().Select(c => new SelectListItem()
            {
                Value = c,
                Text = c,
                Selected = selectedId == c
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Role") { NullDisplayText = " выберите роль" };
            return View("Dropdown");
        }
    }

}
