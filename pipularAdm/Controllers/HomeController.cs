﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Models;
using pipularAdm.Controllers;
using SimpleMembershipTest.Dac;
using pipularAdm.Helpers;

namespace BootstrapMvcSample.Controllers
{
    public class HomeController : BootstrapBaseController
    {
        private static List<HomeInputModel> _models = ModelIntializer.CreateHomeInputModels();
        public HomeController(SimpleMembershipTestDbContext context, ISessionState sessionstate)
            : base(context, sessionstate)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}
