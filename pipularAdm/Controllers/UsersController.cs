﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using Books.DataAccess;
//using Books.Filters;
using Books.Models;
using Omu.ValueInjecter;
using pipularAdm.Filters;
using pipularAdm.Helpers;
using Datatables.Mvc;
using Newtonsoft.Json;
using pipularAdm.Models;
using SimpleMembershipTest.Dac;
using WebMatrix.WebData;

namespace pipularAdm.Controllers
{
    
    [InitializeSimpleMembership]
    //[Authorize(Roles = "администратор")]
    public class UsersController : BootstrapBaseController
    {

        public UsersController(SimpleMembershipTestDbContext context, ISessionState sessionstate)
            : base(context, sessionstate)
        {
        }

        public ActionResult Index()
        {
            HttpCookie myCookie = this.Request.Cookies["Users"];
            var model = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.Columns = new[] { "№", "Имя Пользователя","Email","Роль" };
            model.EntityName = "Users";
            model.EntityNameRus = "пользователя";
            return View(model);

        }

        public ActionResult GetDataTables(Datatables.Mvc.DataTable dataTable)
        {
            var query = _context.UserProfiles.AsQueryable();

            var totalcount = query.ToList().Count();
            if (!String.IsNullOrEmpty(dataTable.sSearch))
            {
                int intval;
                if (int.TryParse(dataTable.sSearch, out intval))
                {
                    query = query.Where(x => x.UserId == intval);
                }
                else
                {
                    query = query.Where(x => x.UserName.StartsWith(dataTable.sSearch));
                }
            }
            var filtercount = query.Count();

            if (dataTable.iSortCols != null && dataTable.iSortCols.Any())
                switch (dataTable.iSortCols[0])
                {
                    case 0:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.UserId) : query.OrderByDescending(x => x.UserId);
                        break;
                    case 1:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.UserName) : query.OrderByDescending(x => x.UserName);
                        break;
                    default:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.UserId) : query.OrderByDescending(x => x.UserId);
                        break;
                }
            query = query.Skip(dataTable.iDisplayStart);
            if (dataTable.iDisplayLength > 0)
            {
                query = query.Take(dataTable.iDisplayLength);
            }
            var table = query.ToList().Select(t =>
                                                  {
                                                      var roles = Roles.GetRolesForUser(t.UserName);
                                                      return new DataTableRow(t.UserId.ToString(), "dtrowclass") { t.UserId.ToString(), t.UserName, t.EMail, roles == null || !roles.Any() ? "" : roles.First() };
                                                  }).ToList();
            HttpCookie myCookie = this.Request.Cookies["Users"] ?? new HttpCookie("Users");
            myCookie.Value = JsonConvert.SerializeObject(new GridState() { iDisplayLength = dataTable.iDisplayLength, iDisplayStart = dataTable.iDisplayStart, aaSorting = "[[ " + dataTable.iSortCols[0] + ", \"" + (dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? "asc" : "desc") + "\" ]]" });
            myCookie.Expires = DateTime.Now.AddDays(365);
            return new DataTableResultExt(dataTable, totalcount, filtercount, table);
        }
        [HttpPost]
        public ActionResult Create(UserProfileInput model)
        {
            if (ModelState.IsValid)
            {
                //var source = new Books.DataAccess.UserProfile();
                //source.InjectFrom(model);
                //source.CreatedOn = DateTime.Now;
                //_context.UserProfiles.Add(source);
                //_context.SaveChanges();
                WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new {EMail = model.EMail}, false);
                if (!String.IsNullOrWhiteSpace(model.Role) )
                {
                    Roles.AddUserToRole(model.UserName, model.Role);
                }
                Success("пользователь сохранен!");
                return RedirectToAction("Index");
            }
            Error("были ошибки в форме ввода.");
            ViewBag.Title = "Новый пользователь";
            //model.Id = 0;
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Новый пользователь";
            var model = new UserProfileInput();
            //model.Id = 0;
            return View(model);
        }

        public ActionResult Delete(short id)
        {
            var model = _context.UserProfiles.FirstOrDefault(w => w.UserId == id);
            if(model==null)
                Attention("нет такого пользователя");
            else
            {
                try
                {
                    // TODO: Add delete logic here
                    if (Roles.GetRolesForUser(model.UserName).Any())
                    {
                        Roles.RemoveUserFromRoles(model.UserName, Roles.GetRolesForUser(model.UserName));
                    }
                    ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(model.UserName); // deletes record from webpages_Membership table
                    ((SimpleMembershipProvider)Membership.Provider).DeleteUser(model.UserName, true); // deletes record from UserProfile table

                }
                catch (Exception ex)
                {
                }
                Information("пользователь удален");
            }
            return RedirectToAction("index");
        }
        public ActionResult Edit(short id)
        {
            var source = _context.UserProfiles.FirstOrDefault(w => w.UserId == id);
            if (source == null)
            {
                Attention("нет такого пользователя");
                return RedirectToAction("index");
            }
            var model = new UserProfileInput();
            model.InjectFrom(source);
            
            ViewBag.Title = source.UserName;
            model.Password = "******";
            var roles = Roles.GetRolesForUser(source.UserName);
            model.Role = roles == null || !roles.Any() ? "" : roles.First();

            return View("Create", model);
        }
        [HttpPost]
        public ActionResult Edit(UserProfileInput model, short id)
        {
            var source = _context.UserProfiles.FirstOrDefault(w => w.UserId == id);
            if (ModelState.IsValid)
            {

                source.EMail = model.EMail;
                source.UserId = (short)id;
                _context.SaveChanges();
                if (model.Password != "******" && !String.IsNullOrWhiteSpace(model.Password))
                {
                    var token = WebSecurity.GeneratePasswordResetToken(model.UserName);
                    WebSecurity.ResetPassword(token, model.Password);
                }
                var roles = Roles.GetRolesForUser(source.UserName);
                var role = roles == null || !roles.Any() ? "" : roles.First();
                if (!String.IsNullOrWhiteSpace(model.Role) && model.Role != role)
                {
                    if (!String.IsNullOrWhiteSpace(role))
                    {
                        Roles.RemoveUserFromRole(source.UserName, role);
                    }
                    Roles.AddUserToRole(source.UserName, model.Role);
                }

                Success("пользователь обновлен!");
                return RedirectToAction("index");
            }
            
            ViewBag.Title = source.UserName;

            return View("Create", model);
        }

        public ActionResult Details(short id)
        {
            var source = _context.UserProfiles.FirstOrDefault(w => w.UserId == id);
            if (source == null)
            {
                Attention("нет такого пользователя");
                return RedirectToAction("index");
            }
            var model = new UserProfile();
            model.InjectFrom(source);
            return View(model);
        }

    }

}
