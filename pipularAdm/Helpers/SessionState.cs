﻿using System.Web;

namespace pipularAdm.Helpers
{
    public interface ISessionState
    {
        void Clear();
        void Delete(string key);
        object Get(string key);
        void Store(string key, object value);
    }

    public class DefaultSessionState : ISessionState
    {
        private readonly HttpSessionStateBase session;

        public DefaultSessionState(HttpSessionStateBase session)
        {
            this.session = session;
        }

        public void Clear()
        {
            session.RemoveAll();
        }

        public void Delete(string key)
        {
            session.Remove(key);
        }

        public object Get(string key)
        {
            return session[key];
        }

        public void Store(string key, object value)
        {
            session[key] = value;
        }
    }

    public static class SessionExtensions
    {
        public static T Get<T>(this ISessionState sessionState, string key) where T : class
        {
            return sessionState.Get(key) as T;
        }
    }
}