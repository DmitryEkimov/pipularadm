﻿using System.ComponentModel.DataAnnotations;
using pipularAdm.Models;

namespace Books.Models
{
    public interface INavModel
    {
        [ScaffoldColumn(false)]
         int? PreviousRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int? NextRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int FirstRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int LastRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        long RN { get; set; }
        [ScaffoldColumn(false)]
        long Total { get; set; }
        [ScaffoldColumn(false)]
        GridState Table { get; set; }
        [ScaffoldColumn(false)]
        string EntityName { get; set; }
    }
}