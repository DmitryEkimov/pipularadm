﻿using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using pipularAdm.Helpers;
using SimpleMembershipTest.Dac;

namespace Books
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            //builder.RegisterType<SmtpClient>().SingleInstance();

            //builder.RegisterType<UserMailer>()
            //    .As<IUserMailer>()
            //    .SingleInstance();


            builder.RegisterControllers(typeof(WebModule).Assembly);
            //builder.RegisterApiControllers(typeof(WebModule).Assembly);
            builder.RegisterType<DefaultSessionState>().As<ISessionState>().InstancePerLifetimeScope();
            builder.Register(c => new HttpSessionStateWrapper(HttpContext.Current.Session)).As<HttpSessionStateBase>().InstancePerLifetimeScope();
            builder.RegisterType<SimpleMembershipTestDbContext>().InstancePerLifetimeScope();
            //.InjectActionInvoker();

            //builder.RegisterType<TransactedActionFilter>()
            //    .AsActionFilterFor<HomeController>()
            //    .InstancePerHttpRequest();

            //builder.RegisterType<Membership>()
            //    .As<IMembership>()
            //    .InstancePerHttpRequest();
            //builder.RegisterAssemblyTypes(typeof(WebModule).Assembly)
            //    .AsClosedTypesOf(typeof(ITypeConverter<,>))
            //    .AsSelf();
            builder.RegisterFilterProvider();

        }
    }
}