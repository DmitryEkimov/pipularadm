﻿# Host: localhost  (Version: 5.6.13)
# Date: 2013-08-11 18:46:05
# Generator: MySQL-Front 5.3  (Build 4.9)

/*!40101 SET NAMES utf8 */;

#
# Source for table "userprofile"
#

CREATE TABLE `userprofile` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` longtext NOT NULL,
  `Email` longtext,
  `Facebook` longtext,
  `Age` int(11) DEFAULT NULL,
  `Rate` double DEFAULT NULL,
  `LastName` longtext,
  `FirstName` longtext,
  `Discriminator` varchar(128) NOT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UserId` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Source for table "webpages_membership"
#

CREATE TABLE `webpages_membership` (
  `UserId` int(11) NOT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `ConfirmationToken` varchar(128) DEFAULT NULL,
  `IsConfirmed` tinyint(1) DEFAULT NULL,
  `LastPasswordFailureDate` datetime DEFAULT NULL,
  `PasswordFailuresSinceLastSuccess` int(11) NOT NULL,
  `Password` varchar(128) DEFAULT NULL,
  `PasswordChangedDate` datetime DEFAULT NULL,
  `PasswordSalt` varchar(128) DEFAULT NULL,
  `PasswordVerificationToken` varchar(128) DEFAULT NULL,
  `PasswordVerificationTokenExpirationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  CONSTRAINT `Membership_UserProfile` FOREIGN KEY (`UserId`) REFERENCES `userprofile` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Source for table "webpages_oauthmembership"
#

CREATE TABLE `webpages_oauthmembership` (
  `Provider` varchar(30) NOT NULL,
  `ProviderUserId` varchar(100) NOT NULL,
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`Provider`,`ProviderUserId`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `OAuthMembership_UserProfile` FOREIGN KEY (`UserId`) REFERENCES `userprofile` (`UserId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Source for table "webpages_oauthtoken"
#

CREATE TABLE `webpages_oauthtoken` (
  `Token` varchar(100) NOT NULL,
  `Secret` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Source for table "webpages_roles"
#

CREATE TABLE `webpages_roles` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`RoleId`),
  UNIQUE KEY `RoleId` (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Source for table "webpages_usersinroles"
#

CREATE TABLE `webpages_usersinroles` (
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `UsersInRoles_Role` (`RoleId`),
  CONSTRAINT `UsersInRoles_Role` FOREIGN KEY (`RoleId`) REFERENCES `webpages_roles` (`RoleId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `UsersInRoles_UserProfile` FOREIGN KEY (`UserId`) REFERENCES `userprofile` (`UserId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
